//
// Created by jonathan.hanks on 7/15/20.
//

#ifndef DAQD_TRUNK_LOCAL_DC_UTILS_H
#define DAQD_TRUNK_LOCAL_DC_UTILS_H

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/*!
 * @brief extract a list of models to run from a systab file
 * @param f The file to read models from
 * @param dest destination buffer
 * @param size size of the destination
 * @return 0 on success
 */
static int
extract_models_from_table( FILE* f, char* dest, int size )
{
    char  hostname[ HOST_NAME_MAX + 1 ];
    char  buffer[ 1024 ];
    char* cur = 0;

    if ( gethostname( hostname, sizeof( hostname ) ) != 0 )
    {
        return 1;
    }
    hostname[ sizeof( hostname ) - 1 ] = '\0';

    while ( fgets( buffer, sizeof( buffer ), f ) != NULL )
    {
        buffer[ sizeof( buffer ) - 1 ] = '\0';
        /* end the line at \n or comments */
        cur = buffer;
        while ( *cur )
        {
            if ( *cur == '\n' || *cur == ';' || *cur == '#' )
            {
                *cur = '\0';
                break;
            }
            ++cur;
        }
        cur = buffer;
        /* find the first space */
        while ( *cur && !isspace( *cur ) )
        {
            ++cur;
        }
        /* move passed the space if we can */
        while ( isspace( *cur ) )
        {
            *cur = '\0';
            ++cur;
        }
        if ( strcmp( hostname, buffer ) == 0 )
        {
            strncpy( dest, cur, size );
            dest[ size - 1 ] = '\0';
            return 0;
        }
    }
    return 1;
}

/*!
 * @brief extract a list of models to run from a systab file
 * @param table The name of the file to read models from
 * @param dest destination buffer
 * @param size size of the destination
 * @return 0 on success
 */
static int
extract_models_from_table_file( const char* table, char* dest, int size )
{
    FILE* f = 0;
    int   rc = 0;

    if ( !table || !dest || size < 100 )
    {
        return 1;
    }
    f = fopen( table, "rt" );
    if ( !f )
    {
        return 1;
    }
    rc = extract_models_from_table( f, dest, size );
    fclose( f );
    return rc;
}

/*!
 * @brief given the tail of a system name + dcu + rate, extract the dcu + rate.
 * @param input The text to parse must be either "model:dcuid:rate" or
 * "model:dcuid"
 * @param dcuIdDest Destination for the dcuid
 * @param rateDest Destination for the rate
 * @note this will always set *dcuIdDest & *rateDest if they are non-null
 * If a dcuid is specified but a rate is not, the rate defaults to 16
 * If a dcuid or rate is invalid it is set to 0
 * If no dcuid (and thus also no rate) is specified then both are set to 0
 */
static void
extract_dcu_rate_from_name( const char* input, int* dcuIdDest, int* rateDest )
{
    size_t size = 0;
    char*  buffer = 0;
    char*  sep = 0;
    int    dcuid = 0;
    int    rate = 0;

    if ( dcuIdDest )
    {
        *dcuIdDest = 0;
    }
    if ( rateDest )
    {
        *rateDest = 0;
    }
    if ( !input )
    {
        return;
    }
    sep = strchr( input, ':' );
    if ( sep == NULL )
    {
        return;
    }
    input = sep + 1;
    size = strlen( input );
    buffer = malloc( size + 1 );
    strncpy( buffer, input, size + 1 );
    sep = strchr( buffer, ':' );
    if ( sep )
    {
        /* dcuid + rate */
        *sep = '\0';
        rate = (int)atoi( sep + 1 );
        if ( *( sep + 1 ) == '\0' )
        {
            rate = 16;
        }
    }
    else
    {
        /* dcuid only, default the rate */
        rate = 16;
    }
    dcuid = (int)atoi( buffer );
    free( buffer );
    if ( dcuIdDest )
    {
        *dcuIdDest = dcuid;
    }
    if ( rateDest )
    {
        *rateDest = rate;
    }
}

/*!
 * @brief Trim off any :dcuid:rate or :dcuid portion from a model name
 * @param name The name to trim
 * @note safe to call with a NULL pointer.  Modifies the string in place.
 */
void
trim_dcuid_and_rate_from_name( char* name )
{
    char* sep = NULL;

    if ( name )
    {
        sep = strchr( name, ':' );
        if ( sep != NULL )
        {
            *sep = '\0';
        }
    }
}

#endif // DAQD_TRUNK_LOCAL_DC_UTILS_H
