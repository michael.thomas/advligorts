/// 	\file ligoPcieTiming.c

#include "ligoPcieTiming.h"

static int
lptcInit( CDS_HARDWARE* pCds, struct pci_dev* lptcdev )
{
    static unsigned int pci_io_addr;
    int                 status = 0;
    char*               addr;
    LPTC_REGISTER*      lptcPtr;
    unsigned int        usec, sec;
    unsigned int        tsec;
    unsigned int        regval;
    int                 ii;

    status = pci_enable_device( lptcdev );
    printk( "Xilinx enabled status = %d\n", status );
    pci_read_config_dword( lptcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    pci_io_addr &= 0xfffffff0;
    addr = (char*)ioremap_nocache( (unsigned long)pci_io_addr, 0x2000 );
    printk( "Xilinx mapped  = 0x%x   0x%p\n", pci_io_addr, addr );
    pCds->lptc = (unsigned int*)addr;
    pCds->ligo_timing_card = 1;

    pCds->gps = (unsigned int*)addr;
    pCds->gpsType = LIGO_RCVR;

    lptcPtr = (LPTC_REGISTER*)addr;

    usec = lptcPtr->gps_nano;
    sec = lptcPtr->gps_sec;
    printk( "Xilinx time  = %u   %u\n", sec, usec );

    printk( "Xilinx status  = 0x%x  \n", lptcPtr->status );
    printk( "Xilinx sw revision  = 0x%x  \n", lptcPtr->revision );
    printk( "Xilinx bp config  = 0x%x  \n", lptcPtr->bp_config );
    printk( "Xilinx bp status  = 0x%x  \n", lptcPtr->bp_status );

    regval = lptcPtr->status;
    if ( regval & LPTC_STATUS_OK )
        printk( "LPTC Status = OK\n" );
    else
        printk( "LPTC Status = BAD \n" );
    if ( regval & LPTC_STATUS_UPLINK_OK )
        printk( "LPTC Uplink = OK\n" );
    else
        printk( "LPTC Uplink = BAD \n" );
    printk( "LPTC Leap Seconds = %d\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) >> 8 ) );
    printk( "LPTC Leap Seconds = 0x%x\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) ) );

    lptcPtr->bp_config = LPTC_CMD_STOP_CLK;
    msleep( 10 );
    regval = lptcPtr->bp_status;
    if ( regval & LPTC_BPS_BP_PRESENT )
        printk( "LPTC backplane present = OK\n" );
    else
        printk( "LPTC IS NOT PRESENT \n" );

    // lptcPtr->slot_info[0].config = 0x10110;
    for ( ii = 0; ii < pCds->ioc_cards; ii++ )
    {
        switch ( pCds->ioc_config[ ii ] )
        {
        case GSC_16AI64SSA:
            lptcPtr->slot_info[ ii ].config =
                ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
            break;
        case GSC_18AI32SSC1M:
            lptcPtr->slot_info[ ii ].config =
                ( LPTC_SCR_ADC_SET | IOC_CLK_FAST );
            break;
        case GSC_16AO16:
        case GSC_18AO8:
        case GSC_20AO8:
        default:
            lptcPtr->slot_info[ ii ].config =
                ( LPTC_SCR_DAC_SET | IOC_CLK_SLOW );
            break;
        }
	if(ii == 0)
            lptcPtr->slot_info[ ii ].config |=  LPTC_SCR_ADC_DT_ENABLE ;
        msleep( 10 );
        regval = lptcPtr->slot_info[ ii ].config;
        printk( "Slot Info %d Config = 0x%x\n", ii, regval );
    }
    msleep( 1000 );
    msleep( 1000 );
}

int
lptc_get_gps_time( CDS_HARDWARE* pCds, unsigned int* sec, unsigned int* nsec )
{
    unsigned int regval;

    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    regval = lptcPtr->gps_nano;
    *sec = lptcPtr->gps_sec;
    *nsec = regval * LPTC_USEC_CONVERT;
    return 0;
}

int
lptc_get_slot_config( CDS_HARDWARE* pCds, int slot )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    return ( lptcPtr->slot_info[ slot ].config & 0xffffff );
}

int
lptc_get_slot_status( CDS_HARDWARE* pCds, int slot )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    return ( lptcPtr->slot_info[ slot ].status & 0xffffff );
}

int
lptc_start_clock( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    lptcPtr->bp_config = 0xc;
    return lptcPtr->bp_status;
}

int
lptc_stop_clock( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    lptcPtr->bp_config = LPTC_CMD_STOP_CLK;
    return lptcPtr->bp_status;
}

int
lptc_get_lptc_status( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    return lptcPtr->status;
}

int
lptc_get_bp_status( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    return lptcPtr->bp_status;
}

int
lptc_get_bp_config( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    return lptcPtr->bp_config;
}

void
lptc_dac_duotone( CDS_HARDWARE* pCds, int setting )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc;
    if ( setting )
    {
        lptcPtr->slot_info[ 0 ].config |= LPTC_SCR_DAC_DT_ENABLE;
        lptcPtr->slot_info[ 1 ].config |= LPTC_SCR_DAC_DT_ENABLE;
    }
    else
    {
        lptcPtr->slot_info[ 0 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
        lptcPtr->slot_info[ 1 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
    }
}
