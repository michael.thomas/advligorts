// LIGO PCIe Timing Card
//
#define LPTC_VID	0x10ee
#define LPTC_TID	0xd8c6


typedef struct SLOT_CONFIG {
	unsigned int config;
	unsigned int phase;
	unsigned int status;
	unsigned int reserved;
}SLOT_CONFIG;
typedef struct LPTC_REGISTER {
	unsigned int gps_nano;		// 0x0000
	unsigned int gps_sec;		// 0x0004
	unsigned int status;		// 0x0008
	unsigned int revision;		// 0x000c
	unsigned int bp_config;		// 0x0010
	unsigned int wd_reset;		// 0x0014
	unsigned int bp_status;		// 0x0018
	unsigned int reserved3;		// 0x001c
	SLOT_CONFIG slot_info[10];	// 0x0020
}LPTC_REGISTER;
// Data read from LPTC at 0x1000 offset
typedef struct LPTC_DIAG_INFO {
	unsigned int board_id;		// 0x1000
	unsigned int board_sn;		// 0x1004
	unsigned int sftware_id;	// 0x1008
	unsigned int sftware_rev;	// 0x100c
	unsigned int gps_seconds;	// 0x1010
	unsigned int mod_address;	// 0x1014
	unsigned int board_status;	// 0x1018
	unsigned int board_config;	// 0x101c
}LPTC_DIAG_INFO;

#define LPTC_USEC_CONVERT		0.00011920929

#define LPTC_SCR_IDLE_HIGH		0x1000
#define LPTC_SCR_CLK_INVERT		0x200
#define LPTC_SCR_CLK_ENABLE		0x100
#define LPTC_SCR_ADC_SET		0x1300
#define LPTC_SCR_DAC_SET		0x1100
#define LPTC_SCR_ADC_DT_ENABLE		0x20000
#define LPTC_SCR_DAC_DT_ENABLE		0x40000

#define LPTC_STATUS_OK 			0x80000000
#define LPTC_STATUS_ROOT_NODE 		0x40000000
#define LPTC_STATUS_SUP_FANOUT 		0x20000000
#define LPTC_STATUS_UPLINK_OK 		0x10000000
#define LPTC_STATUS_UPLINK_LOSS 	0x8000000
#define LPTC_STATUS_OCXO_LOCK	 	0x4000000
#define LPTC_STATUS_GPS_LOCK	 	0x2000000
#define LPTC_STATUS_VCXO_VOOR	 	0x1000000
#define LPTC_STATUS_UTC_TIME	 	0x800000
#define LPTC_STATUS_LSEC_DECODE	 	0x400000
#define LPTC_STATUS_LSEC_SUB_PEND	0x200000
#define LPTC_STATUS_LSEC_ADD_PEND	0x100000
#define LPTC_STATUS_LEAP_SEC		0xff00

#define LPTC_BPS_BP_PRESENT		0x200
#define LPTC_BPS_BP_REV			0x18
#define LPTC_BPS_CLK_ACTIVE		0x1
#define LPTC_BPS_CLK_RUN		0x2
#define LPTC_BPS_WD_MON			0x4

#define LPTC_CMD_STOP_CLK		0x0
#define LPTC_CMD_START_CLK		0x1c
#define LPTC_ENABLE_DAC_DUO		0x4000

#define LIGO_RCVR			0x3
